The MessageManager is a library to simple be able to manage messages.

## Usage

```java
// First obtain an instance of the MessageManager
MessageManager msgManager = new MessageManager();

// Set the MessageStorageType
msgManager.setStorageType(MessageStorageType.JSON);

// Set the folder to which messages read from 
msgManager.setMessageFolder(new File("languages"));

// Add here your messages, see the lower section

// Load the messages
manager.load();
```

### Add messages
```java
Message msg = new Message("id", "value");
msg.inLocale(new Locale("en")) // Optional, change the language


// Use MessageManager#addMessage to add the message
```

### With variables
```java
msg.addVariable("id", "defaultValue"); // defaultValue can be left empty, but should be set
```

### Get messages
```java
msg.getMessage("id", new Locale("en")) // Locale is optional and can be set if message needs to be requested in another language than the one set in MessageManager#setDefaultLocale

// To get a parsed message (variables are replaced) make a HashMap
HashMap<String, String> id = new HashMap<>();
id.put("id", "toReplaceTo");
msg.getParsedMessage("id", new Locale("en"), id)) // Locale is optional, as above
```
